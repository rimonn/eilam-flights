from Models.Flight import Flight
from Models.Person import Person

print("Welcome aboard")

# flights
flight01 = Flight('USA', 'Boing')
flight02 = Flight('UK', 'Airbus')

# passengers
passenger01 = Person('Eilam')
passenger02 = Person('Nadav')

# assign passengers to flights
flight01.onboard_new_passenger(passenger01)
flight02.onboard_new_passenger(passenger02)

# show flights occupancy
flight01.print_seats()
flight02.print_seats()
