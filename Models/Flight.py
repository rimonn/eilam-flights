class Flight:
    EMPTY_SEAT = 'Empty'

    def __init__(self, flight_dest, company):
        self.flight_dest = flight_dest
        self.company = company
        self.seats = {}
        self.init_seats()

    def init_seats(self):
        num_of_rows = 10 if self.company == 'Boing' else 15
        columns = 'ABCDE' if self.company == 'Boing' else 'ABCDEFG'
        for i in range(1, num_of_rows):
            for j in columns:
                self.seats.update({str(j) + str(i): self.EMPTY_SEAT})

    def onboard_new_passenger(self, passenger):
        for key, value in self.seats.iteritems():
            if value == self.EMPTY_SEAT:
                self.seats.update({key: passenger})
                passenger.flight = self
                break

    def print_seats(self):
        print("\n" + self.company + " flight to " + self.flight_dest + ":")
        print("----------------------")
        for seat, passenger in self.seats.iteritems():
            print('Seat ' + seat + ": " + str(passenger))
